#include <memory>
#include <iostream>

//Forward declaration is possible as pointers default to nullptr
struct B;

struct A 
{
    std::shared_ptr<B> m_b;
    ~A() 
    {
        std::cout<<"A was destroyed.";
    }
};

struct B
{
    std::shared_ptr<A> m_a;
    ~B()
    {
        std::cout<<"B was destroyed.";
    }
};

int main()
{
    auto a = std::make_shared<A>();
    auto b = std::make_shared<B>();
    
    a->m_b  = b;
    b->m_a = a;
    return 0;
}