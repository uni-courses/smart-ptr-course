#include <memory>
#include <iostream>

void func(std::shared_ptr<int> sPtr, std::unique_ptr<int> uPtr)
{
	std::cout << "Shared pointer reference count (INSIDE): " << sPtr.use_count() << std::endl;

	if (uPtr)
		std::cout << "Unique pointer value: " << *uPtr << std::endl;
	else
		std::cout << "Unique pointer is empty." << std::endl;
}

struct S
{
	int x1, x2;

	S(int x1, int x2) : x1(x1), x2(x2) {}
};

int main()
{
	// std::shared_ptr<int> shared = std::shared_ptr<int>(new int(5));
	std::shared_ptr<int> shared = std::make_shared<int>(5); // optim fata de constructorul de la shared_ptr

	std::cout << "Shared pointer reference count (MAIN): " << shared.use_count() << std::endl;

	std::unique_ptr<int> unique = std::make_unique<int>(6); // e echivalent cu constructorul de la unique_ptr

	if (unique) // convertibil la bool
		std::cout << "Unique pointer value: " << *unique << std::endl;
	else
		std::cout << "Unique pointer is empty." << std::endl;

	//func(shared, unique); // asa nu
	func(shared, std::move(unique));
	
	std::cout << "Shared pointer reference count (MAIN): " << shared.use_count() << std::endl;

	if (unique)
		std::cout << "Unique pointer value: " << *unique << std::endl;
	else
		std::cout << "Unique pointer is empty." << std::endl;

	
	std::shared_ptr<S> sPtr = std::make_shared<S>(1, 2);
	std::unique_ptr<S> uPtr = std::make_unique<S>(2, 3);

	std::cout << "Part of: " << uPtr.get()->x1;

	return 0;
}